function [t,xx, lipid_intake]=coupled_population(diet, N, a, t_max)


% non-coupled model for N cells
% with parameter 'a' normally distributed

% parameters
kappa_r = 200;
n = 3;
kappa_l = 0.001; % same as in figure 5
B = 125;
b = 0.27;
kappa_t = 0.025; % same as in black curve in figure 3
tau = 0.1;
V_l = 1.091*10^6;
V_0 = 14137; % such that r_0=20
k = 6; % term for lipid burning without entering adipocyte
%k=0.01

r_ini1 = 28;
r_ini2 = 91;
%48
l_ini1 = 0.03;
l_ini2 = 2.7;
%0.4
L_ini = 0; %marche pour N=2 mais pas bimodal
%500*2000


% Set a_norm a vector of normal distribution

options = odeset('OutputFcn',@odeplot,'OutputSel',[1 2]);

% ode parameters
CI = [ones(1,5*N/8)*l_ini1 ones(1,3*N/8)*l_ini2 ones(1,5*N/8)*r_ini1 ones(1,3*N/8)*r_ini2 L_ini]'; % initial conditions
tspan = [0 t_max]; % time span

% solving of the system for each cell

[t,xx] = ode15s(@monedo,tspan,CI,options);
l_conv = xx(end,1:N); % store last value of l
r_conv = xx(end,N+1:2*N); % same for r
L_conv = xx(end,2*N+1);

mean(l_conv)
mean(r_conv)

% TODO : use this value to make a fancy plot
r_out = xx(:,N+1:2*N);
L_out = xx(:,2*N+1);
size(r_out)
size(repmat(a',length(t),1))
lipid_intake = sum(repmat(a',length(t),1).*r_out.^2*(kappa_r)^n./(kappa_r^n+r_out.^n),2).*(L_out./(L_out+N*kappa_l));




subplot(2,3,1)
hist(l_conv,50)
xlabel('Intracellular lipid content')
ylabel('Number of cells')
title('Distribution of converging intracellular lipid content')
subplot(2,3,2)
hist(r_conv)
xlabel('Adipocyte radius')
ylabel('Number of cells')
title('Distribution of converging adipocyte radius')
subplot(2,3,3)
hist(a)
xlabel('a')
ylabel('Number of cells')
title('Maximal lipogenesis flux distribution a')
subplot(2,3,4)
plot(t,xx(:,2*N+1))
xlabel('Time')
ylabel('Extracellular lipid content')
title('Trajectory')
subplot(2,3,5)
plot(t,xx(:,N+1:2*N))
xlabel('Time')
ylabel('Adipocyte radius')
title('Trajectory')
subplot(2,3,6)
plot(t,xx(:,1:N))
xlabel('Time')
ylabel('Intracellular lipid content')
title('Trajectory')

function xxp = monedo(t,xx)  % nested function
% system of equation (1) (3)

if(diet == 1)
    % typical diet, high calorie diet, back to the typical diet and restrictive diet
    A_1 = 0.42*(t<10000) + 0.57*(t>=10000)*(t<20000) + 0.42*(t>=20000)*(t<30000) + 0.35*(t>=30000);
end



% yoyo diet
% vec = [0.37, 0.67];
% A_1 = repelem(repmat(vec, 1, 500), 5);
if(diet == 2)
    high = 0.47;
    low = 0.37;
    A_1 = 0.42*(t<500) + 0.42*(t>=500)*(t<1000) + 0.42*(t>=1000)*(t<1500) + 0.47*(t>=1500)*(t<2000) + 0.37*(t>=2000)*(t<2500) + 0.47*(t>=2500)*(t<3000) + 0.37*(t>=3000)*(t<3500) + 0.47*(t>=3500)*(t<4000) + 0.37*(t>=4000)*(t<4500) + 0.47*(t>=4500)*(t<5000) + 0.37*(t>=5000)*(t<5500) + 0.47*(t>=5500)*(t<6000) + 0.37*(t>=6000)*(t<6500) + 0.47*(t>=6500)*(t<7000) + 0.37*(t>=7000)*(t<7500) + 0.47*(t>=7500)*(t<8000) + 0.37*(t>=8000)*(t<8500) + 0.47*(t>=8500)*(t<9000) + 0.37*(t>=9000)*(t<9500) + 0.47*(t>=9500)*(t<10000);
    %A_1 =  0.47*(t<500) + 0.37*(t>=500)*(t<1000) + high*(t>=1000)*(t<1500) + low*(t>=1500)*(t<2000) + high*(t>=2000)*(t<2500) + low*(t>=2500)*(t<3000) + high*(t>=3000)*(t<3500) + low*(t>=3500)*(t<4000) + high*(t>=4000)*(t<4500) + low*(t>=4500)*(t<5000);
end

% constant diet
if(diet == 3)
    A_1 = 0.42;
end



%variables
l = xx(1:N);
r = xx(N+1:2*N);
L = xx(2*N+1);

xxp = [ a.*r.^2*(L/(L+N*kappa_l))*(kappa_r)^n./((kappa_r)^n+r.^n) - (B+b*r.^2).*l./(kappa_t+l) ;
        (1/tau)*((V_l*l+V_0)./(4*pi*r.^2)-r./3); 
        sum( - a.*r.^2*(L/(L+N*kappa_l))*(kappa_r)^n./(kappa_r^n+r.^n) ...
        + (B+b*r.^2).*l./(kappa_t+l)) ...
        + A_1 - k*L];
end
end