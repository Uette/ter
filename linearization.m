%%%%% Function to find the eigenvalues and eigenvectors of the Jacobian matrix at a given (l, r) point %%%%%
% 8/05/2017

% parameters of the function
% l : amount of triglyceride stored in the adipocyte
% r : radius of the adipocyte
% a : lipogenesis maximal flux

function[V, D] = linearization(l, r, L)

    % parameters of the model
    a = 0.5;
    kappa_r = 200;
    n = 3;
    B = 125;
    b = 0.27;
    kappa_l = 0.001; % same as in figure 5 of the paper by Soula
    kappa_t = 0.025; % same as in black curve in figure 3
    tau = 0.1;
    V_l = 1.091*10^6;
    V_0 = 14137;  % such that r_0 = 20

    % partial derivatives: dl/dt=f_1(l,r), dr/dt = f_2(l, r)
    df1_dl = @(l,r) -(B+b*r^2)*(kappa_t)/((kappa_t+l)^2);
    df1_dr = @(l,r) a*(L/(L+kappa_l))*(kappa_r)^n*(2*r*((kappa_r)^n+r^n)-r^2*n*r^(n-1))/((kappa_r)^n+r^n)^2 - b*l*2*r/(kappa_t+l);
    df2_dl = @(l,r) V_l/(4*pi*r^2*tau);
    df2_dr = @(l,r) (1/tau)*(-2*(V_l*l+V_0)/(4*pi*r^3)-(1/3));

    % Jacobian matrix
    J = @(l,r)[df1_dl(l,r) df1_dr(l,r) ; df2_dl(l,r) df2_dr(l,r)];

    % eigenvalues and eigenvectors
    [V,D] = eig(J(l, r));

end