function [l_conv,r_conv] = run_monedo()
    % simulation of the equation system (1) and (3)

    % integration parameters
    CI = [0.03; 20]; % initial conditions
    tspan = [0 50]; % time span
        
    % parameters of the system
    % the variables are shared with the subfunction monedo
    a=0.5; % diffusion rate of the triglycerides (influx)
    kappa_r = 200;
    n = 3;
    kappa_l = 0.001; % same as in figure 5
    B = 125;
    b = 0.27;
    kappa_t = 0.025; % same as in black curve in figure 3
    tau = 0.1;
    V_l = 1.091*10^6;
    V_0 = 14137; % such that r_0=20
    % parameter L: available extra cellular lipid content
    number_L = 100;
    gamma_values = linspace(1.05,1.22,number_L)*b/a;
    L_values=gamma_values*kappa_l./(1-gamma_values);
    pars = [L_values(1:end-1) fliplr(L_values)];
    
    
    % solving of the system for increasing and then decreasing values of L
    l_conv = zeros(2*number_L-1,1);
    r_conv = zeros(2*number_L-1,1);
    for i = 1:(2*number_L-1)
        L = pars(i); % update L value
        [t,xx] = ode45(@monedo,tspan,CI);
        l_conv(i) = xx(end,1); % store last value of l
        r_conv(i) = xx(end,2); % same for r
        CI = [l_conv(i),r_conv(i)]; % update the initial conditions with them
    end

    % plot
    xint = pars;
    yint1 = l_conv;
    yint2 = r_conv;

    plot(xint,yint1, 'LineWidth', 2, 'Color', 'b')
    xlabel('Available extracellular lipid content L')
    ylabel('Approximate intracellular lipid amount l at t = 50')
    set(gca, 'FontSize', 20)
    grid on
    print('-depsc', 'hysteresis_l.eps')


    %title({'Approximated convergence values of the stored triglyderides amount l', 'for increasing and then decreasing extracellular lipid content'})

%     plot(xint,yint2, 'LineWidth', 2, 'Color', 'b')
%     xlabel('Available extracellular lipid content L')
%     ylabel('Approximate cell radius r at t = 50')
%     set(gca, 'FontSize', 20)
%     grid on
%     print('-depsc', 'hysteresis_r.eps')


    %title({'Approximated convergence values of the cell radius r', 'for increasing and then decreasing extracellular lipid content'})


    function xxp = monedo(~,xx)  % nested function
        % system of equation (1) (3)
        
        % variables
        l = xx(1);
        r = xx(2);
               
        xxp = [ a*r^2*(L/(L+kappa_l))*(kappa_r)^n/((kappa_r)^n+r^n) - (B+b*r^2)*l/(kappa_t+l) ;
            (1/tau)*((V_l*l+V_0)/(4*pi*r^2)-r/3)];
        
        
    end                

end