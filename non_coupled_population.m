function [l_bounds, r_bounds] = non_coupled_population()

% non-coupled model for N cells
% with parameter 'a' normally distributed

% parameters
L = 1.63*10^(-3);
kappa_r = 200;
n = 3;
kappa_l = 0.001; % same as in figure 5
B = 125;
b = 0.27;
kappa_t = 0.025; % same as in black curve in figure 3
tau = 0.1;
V_l = 1.091*10^6;
V_0 = 14137; % such that r_0=20

r_ini = 48;
l_ini = 0.4;

% Set a_norm a vector of normal distribution

% variables
mu = 0.5;
sigma = 0.02;
N = 200;

a = abs(normrnd(mu,sigma,N,1));

% ode parameters
CI = [ones(1,N)*l_ini ones(1,N)*r_ini]'; % initial conditions
tspan = [0 50]; % time span

% solving of the system for each cell

[t,xx] = ode45(@monedo,tspan,CI);
l_conv = xx(end,1:N); % store last value of l
r_conv = xx(end,N+1:2*N); % same for r

l_1 = min(l_conv);
l_2 = max(l_conv(l_conv<1));
l_3 = min(l_conv(l_conv>1));
l_4 = max(l_conv);
l_bounds = [l_1 l_2 l_3 l_4];
r_1 = min(r_conv);
r_2 = max(r_conv(r_conv<60));
r_3 = min(r_conv(r_conv>60));
r_4 = max(r_conv);
r_bounds = [r_1 r_2 r_3 r_4];


nbins = 30; % number of class for the histograms
h = histogram(l_conv, nbins);
h.FaceColor = 'b';
xlabel('Intracellular lipid content l')
ylabel('Number of cells')
set(gca,'FontSize', 20)
%title('Distribution of converging intracellular lipid content')
print('-depsc', 'non_coupled_l.eps')

h = histogram(r_conv, nbins)
h.FaceColor = 'b';
xlabel('Adipocyte radius r')
ylabel('Number of cells')
set(gca,'FontSize', 20)
%title('Distribution of converging adipocyte radius')
print('-depsc', 'non_coupled_r.eps')

h = histogram(a);
h.FaceColor = 'b';
xlabel('Lipogenesis maximal flux a')
ylabel('Number of cells')
set(gca,'FontSize', 20)
%title('Maximal lipogenesis flux distribution a')
print('-depsc', 'non_coupled_a.eps')

function xxp = monedo(~,xx)  % nested function
% system of equation (1) (3)

%variables
l = xx(1:N);
r = xx(N+1:2*N);

xxp = [ a.*r.^2*(L/(L+kappa_l))*(kappa_r)^n./((kappa_r)^n+r.^n) - (B+b*r.^2).*l./(kappa_t+l) ;
    (1/tau)*((V_l*l+V_0)./(4*pi*r.^2)-r./3)];


end

end
