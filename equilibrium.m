%%%%% Equilibrium of the one cell model anf their stability by finding the roots of the polynomial in r %%%%%
% 8/05/2017

% parameters of the model
a = 0.5; % lipogenesis maximal flux
kappa_r = 200;
n = 3;
B = 125;
b = 0.27;
kappa_t = 0.025; % same as in black curve in figure 3
kappa_l = 0.001; % same as in figure 5 of the paper by Soula
tau = 0.1;
V_l = 1.091*10^6;
V_0 = 14137; % such that r_0=20

number_L = 1000;
gamma_values = linspace(1.05,1.22,number_L)*b/a;
L_values = gamma_values*kappa_l./(1-gamma_values);

l_equi = @(r) (4*pi*r^3 - 3*V_0)/(3*V_l); % function to find the value of l at equilibrium given r

fixed_points = NaN(3*number_L, 4); % the first column is for the value of L
% the second and third for the value of r and l
% the fourth contains 1 if the equilibrium is stable and 0 otherwise

iter = 1;
for L = L_values
    
    v = [-b*4*pi
        0
        -B*4*pi
        b*3*V_0+ a*(L/(L+kappa_l))*(kappa_r)^3*4*pi - b*4*pi*(kappa_r)^3
        0
        -B*4*pi*(kappa_r)^3-3*V_0
        a*(L/(L+kappa_l))*(kappa_r)^3*3*V_l*kappa_t-3*V_0*a*(L/(L+kappa_l))*(kappa_r)^3+b*3*V_0*(kappa_r)^3
        0
        B*3*V_0*(kappa_r)^3]; % coefficients of the polynomial
    res = roots(v);
    res_real = res(imag(res)==0);
    res_pos = res_real((res_real)>=0)'; % keep only positive and real roots
    
    for r = res_pos
        l = l_equi(r);
        [V, D] = linearization(l, r, L);
        stability = (D(1, 1)<0 && D(2, 2)<0);
        fixed_points(iter, :) = [L r l stability];
        iter = iter + 1;        
    end
end



% plot for r
stable_low_r = fixed_points(((fixed_points(:,4)==1) & (fixed_points(:, 2)<40)), 1:2);
stable_high_r = fixed_points(((fixed_points(:,4)==1) & (fixed_points(:, 2)>40)), 1:2);
unstable_r = fixed_points((fixed_points(:,4)==0), 1:2);


hold on
plot(stable_high_r(:,1), stable_high_r(:,2), '-', 'LineWidth', 2, 'Color', 'b')
plot(unstable_r(:,1), unstable_r(:,2), ':', 'Color', 'b')
hleg = legend('Stable equilibrium', 'Unstable equilibrium');
hleg.FontSize = 20;
set(hleg,'Location', 'Best')
plot(stable_low_r(:,1), stable_low_r(:,2),'-', 'LineWidth', 2, 'Color', 'b')

%title('Equilibrium and their stability for L between 0.0013 and 0.0019')
xlabel('Available extracellular lipid content L')
ylabel('Adipocyte radius r')
set(gca, 'FontSize', 20)
grid on

% save figure
% print('-depsc', 'equilibrium_r.eps')



% plot for l
stable_low_l = fixed_points(((fixed_points(:,4)==1) & (fixed_points(:, 3)<0.5)), [1 3]);
stable_high_l = fixed_points(((fixed_points(:,4)==1) & (fixed_points(:, 3)>0.5)), [1 3]);
unstable_l = fixed_points((fixed_points(:,4)==0), [1 3]);

close all
hold on
plot(stable_high_l(:,1), stable_high_l(:,2), '-', 'LineWidth', 2, 'Color', 'b')
plot(unstable_l(:,1), unstable_l(:,2), ':', 'Color', 'b')
hleg = legend('Stable equilibrium', 'Unstable equilibrium');
hleg.FontSize = 20;
set(hleg,'Location', 'Best')
plot(stable_low_l(:,1), stable_low_l(:,2),'-', 'LineWidth', 2, 'Color', 'b')

%title('Equilibrium and their stability for L between 0.0013 and 0.0019')
xlabel('Available extracellular lipid content L')
ylabel('Amount of stored triglycerides l')
set(gca, 'FontSize', 20)
grid on

% save figure
% print('-depsc', 'equilibrium_l.eps')













