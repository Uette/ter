N = 40; % size of the population
nbins = 30; % number of classes for histograms
mu = 0.5;
sigma = 0.02;
a = abs(normrnd(mu,sigma,N,1));

% distribution of a
h = histogram(a, nbins)
h.FaceColor = 'b';
xlabel('Lipogenesis maximal flux a')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'hist_a.eps')


%%%% diet 1 : low - high - low - very low
[t,xx, lipid_intake] = coupled_population(1, N, a, 40000);
clf

% distributions at different times
t_1 = sum(t<7500);
t_2 = sum(t<18000);
t_3 = sum(t<29000);
t_4 = sum(t<40000);


% initial distribution after stabilization
% l
 % TODO: find nicely this time
h = histogram(xx(t_1, 1:N), nbins);
h.FaceColor = 'b';
xlabel('Intracellular lipid content l')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_1_l.eps')

% r
h = histogram(xx(t_1, N+1:2*N), nbins);
h.FaceColor = 'b';
xlabel('Adipocyte radius r')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_1_r.eps')
% proportion
p_1_high = (sum(xx(t_1, N+1:2*N)>50))/40;

% distribution after stabilization at high level
% l
 % TODO: find nicely this time
h = histogram(xx(t_2, 1:N), nbins);
h.FaceColor = 'b';
xlabel('Intracellular lipid content l')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_2_l.eps')

% r
h = histogram(xx(t_2, N+1:2*N), nbins);
h.FaceColor = 'b';
xlabel('Adipocyte radius r')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_2_r.eps')

% proportion
p_2_high = (sum(xx(t_2, N+1:2*N)>50))/40;

% distribution after going back to the initial diet

% l
h = histogram(xx(4800, 1:N), nbins);
h.FaceColor = 'b';
xlabel('Intracellular lipid content l')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_3_l.eps')

% r
h = histogram(xx(t_3, N+1:2*N), nbins);
h.FaceColor = 'b';
xlabel('Adipocyte radius r')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_3_r.eps')

% proportion
p_3_high = (sum(xx(t_3, N+1:2*N)>50))/40;


% distribution after reducing lipid incomes
%l
h = histogram(xx(end, 1:N), nbins);
h.FaceColor = 'b';
xlabel('Intracellular lipid content l')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_4_l.eps')

% r
h = histogram(xx(end, N+1:2*N), nbins);
h.FaceColor = 'b';
xlabel('Adipocyte radius r')
ylabel('Number of cells')
set(gca,'FontSize', 20)
print('-depsc', 'dist_4_r.eps')

% proportion
p_4_high = (sum(xx(end, N+1:2*N)>50))/40;



% total volume
vol = 4/3*pi.*xx(:, N+1:2*N).^3;
plot(t, sum(vol,2), 'LineWidth', 2, 'Color', 'b')
xlabel('Time')
ylabel('Total adipocyte volume (\mu m^3)')
set(gca,'FontSize', 20)
grid on
print('-depsc', 'total_volume.eps')


% trajectories

%l
plot(t, xx(:,1:N), 'LineWidth', 2)
xlabel('Time')
ylabel('Intracellular lipid content l_i')
set(gca,'FontSize', 20)
xticks([0 0.5*10^(4) t(t_1) 1*10^(4) 1.5*10^(4) t(t_2) 2*10^(4) 2.5*10^(4) t(t_3) 3*10^(4) 3.5*10^(4) t(t_4)]);
xticklabels({'0', '0.5', 't_1', '1', '1.5', 't_2', '2', '2.5', 't_3', '3', '3.5', 't_4'})
grid on
text(40200, -0.2 ,'x 10^4', 'Fontsize', 20)
print('-depsc', 'traj_l.eps')

%r
plot(t, xx(:,N+1:2*N), 'LineWidth', 2)
xlabel('Time')
ylabel('Adipocyte radius r_i')
set(gca,'FontSize', 20)
xticks([0 0.5*10^(4) t(t_1) 1*10^(4) 1.5*10^(4) t(t_2) 2*10^(4) 2.5*10^(4) t(t_3) 3*10^(4) 3.5*10^(4) t(t_4)]);
xticklabels({'0', '0.5', 't_1', '1', '1.5', 't_2', '2', '2.5', 't_3', '3', '3.5', 't_4'})
grid on
text(40200, 19.8 ,'x 10^4', 'Fontsize', 20)
print('-depsc', 'traj_r.eps')

% plot of the diet
A_1 = 0.42.*(t<10000) + 0.6.*(t>=10000).*(t<20000) + 0.42.*(t>=20000).*(t<30000) + 0.36.*(t>=30000);
% TODO : get nicely
plot(t, A_1, 'LineWidth', 2, 'Color', 'b')
ylim([0.35 0.65])
grid on
xlabel('Time')
ylabel('Source term s (nmol*h^{-1})')
set(gca,'FontSize', 20)
print('-depsc', 'diet_1.eps')

%%%% diet 2 : yoyo diet

[t2,xx2, lipid_intake] = coupled_population(2, N, a, 10000);
vol2 = 4/3*pi.*xx2(:, N+1:2*N).^3;

%%%% diet 3 : constant diet

[t3, xx3, lipid_intake] = coupled_population(3, N, a, 10000);
% total volume
vol3 = 4/3*pi.*xx3(:, N+1:2*N).^3;


h = histogram(xx3(end, N+1:2*N), nbins);
h.FaceColor = 'b';
xlabel('Adipocyte radius r')
ylabel('Number of cells')
set(gca,'FontSize', 20)

% plot of the diets
A_2 = 0.42*(t2<500) + 0.42*(t2>=500).*(t2<1000) + 0.42*(t2>=1000).*(t2<1500) + 0.47*(t2>=1500).*(t2<2000) + 0.37*(t2>=2000).*(t2<2500) + 0.47*(t2>=2500).*(t2<3000) + 0.37*(t2>=3000).*(t2<3500) + 0.47*(t2>=3500).*(t2<4000) + 0.37*(t2>=4000).*(t2<4500) + 0.47*(t2>=4500).*(t2<5000) + 0.37*(t2>=5000).*(t2<5500) + 0.47*(t2>=5500).*(t2<6000) + 0.37*(t2>=6000).*(t2<6500) + 0.47*(t2>=6500).*(t2<7000) + 0.37*(t2>=7000).*(t2<7500) + 0.47*(t2>=7500).*(t2<8000) + 0.37*(t2>=8000).*(t2<8500) + 0.47*(t2>=8500).*(t2<9000) + 0.37*(t2>=9000).*(t2<9500) + 0.47*(t2>=9500);
A_3 = 0.42*ones(1, length(t2));
% TODO : get nicely
clf
hold on
plot(t2, A_2, 'LineWidth', 2, 'Color', 'b')
plot(t2, A_3, 'LineWidth', 2)
ylim()
grid on
xlabel('Time')
ylabel('Source term s (nmol*h^{-1})')
set(gca,'FontSize', 20)
hleg = legend('Yoyo diet', 'Constant diet')
set(hleg,'Location', 'Northwest')
print('-depsc', 'diet_yoyo.eps')


%%%% compare evolution of total volume
clf
hold on
plot(t2, sum(vol2,2), 'LineWidth', 2)
plot(t3, sum(vol3,2), 'LineWidth', 2)
xlim([1500 8000])

xlabel('Time')
ylabel('Total adipocyte volume (\mu m^3)')
set(gca,'FontSize', 20)
hleg = legend('Yoyo diet', 'Constant diet')
set(hleg,'Location', 'Northwest')
grid on
print('-depsc', 'total_volume_yoyo.eps')
