% Finding critical points for extracellular lipid content L

% expressing L as a function of r and finding points where the derivative
% is zero.

clf

% parameters
a=0.5; % diffusion rate of the triglycerides (influx)
kappa_r = 200;
n = 3;
kappa_l = 0.001; % same as in figure 5
B = 125;
b = 0.27;
kappa_t = 0.025; % same as in black curve in figure 3
tau = 0.1;
V_l = 1.091*10^6;
V_0 = 14137; % such that r_0=20

% declaration of a symbolic function
% variable r
syms r
% function p (L as a function of r)
p = kappa_l.*((r.^8*(-b*4*pi)+r.^6*(-B*4*pi)+r.^5*(b*V_0*3-b*4*pi*(kappa_r)^3)+r.^3*(-B*4*pi*(kappa_r)^3+3*B*V_0)+r.^2*(b*3*V_0*(kappa_r)^3)+B*3*V_0*kappa_r^3) ./ (r.^5*(-a*(kappa_r)^3*4*pi)+r.^2*(-a*(kappa_r)^3*3*V_l*kappa_t+3*V_0*a*(kappa_r)^3)))...
    ./(1-((r.^8*(-b*4*pi)+r.^6*(-B*4*pi)+r.^5*(b*V_0*3-b*4*pi*(kappa_r)^3)+r.^3*(-B*4*pi*(kappa_r)^3+3*B*V_0)+r.^2*(b*3*V_0*(kappa_r)^3)+B*3*V_0*kappa_r^3) ./ (r.^5*(-a*(kappa_r)^3*4*pi)+r.^2*(-a*(kappa_r)^3*3*V_l*kappa_t+3*V_0*a*(kappa_r)^3))))

% find p_prime the derivative of p
p_prime = diff(p)

% roots of the differential
crit_pts = solve(p_prime)

% keeping real roots
crit_pts_real = crit_pts(imag(crit_pts)==0)

% value of p at our critical points
L_crit = double(subs(p,crit_pts_real))


% using the classical array type function declaration for the plot, we declare p_2
p_2 = @(r) kappa_l.*((r.^8*(-b*4*pi)+r.^6*(-B*4*pi)+r.^5*(b*V_0*3-b*4*pi*(kappa_r)^3)+r.^3*(-B*4*pi*(kappa_r)^3+3*B*V_0)+r.^2*(b*3*V_0*(kappa_r)^3)+B*3*V_0*kappa_r^3) ./ (r.^5*(-a*(kappa_r)^3*4*pi)+r.^2*(-a*(kappa_r)^3*3*V_l*kappa_t+3*V_0*a*(kappa_r)^3)))...
    ./(1-((r.^8*(-b*4*pi)+r.^6*(-B*4*pi)+r.^5*(b*V_0*3-b*4*pi*(kappa_r)^3)+r.^3*(-B*4*pi*(kappa_r)^3+3*B*V_0)+r.^2*(b*3*V_0*(kappa_r)^3)+B*3*V_0*kappa_r^3) ./ (r.^5*(-a*(kappa_r)^3*4*pi)+r.^2*(-a*(kappa_r)^3*3*V_l*kappa_t+3*V_0*a*(kappa_r)^3))))

% number of plotting points
npt=50;
% interval of r for plot
r = linspace(20,125,npt);

plot(r,p_2(r),'LineWidth',2, 'Color', 'b')

first = [linspace(20, 31.87, 1000) 31.87*ones(1, 1000)];
L_first = [p_2(31.87)*ones(1, 1000) linspace(p_2(31.87), 0.0008, 1000)];
hold on
plot(first, L_first, '--', 'Color', 'r', 'LineWidth',2)

second = [linspace(20, 68.91, 1000) 68.91*ones(1, 1000)];
L_second = [p_2(68.91)*ones(1, 1000) linspace(p_2(68.91), 0.0008, 1000)];
hold on
plot(second, L_second, '--', 'Color', 'r', 'LineWidth',2)


%title('fixed points: L as a function of r')
x = xlabel('Adipocyte radius r at equilibrium')
set(x, 'Units', 'Normalized', 'Position', [0.5, -0.08, 0]);
y = ylabel('Available extracellular lipid content L')
set(y, 'Units', 'Normalized', 'Position', [-0.08, 0.5, 0]);
set(gca,'FontSize', 20)
grid on
yticks([0.0008:0.0002:0.0014 p_2(68.91) p_2(31.87) 0.0020:0.0002:0.0024 ]);
xticks([20 31.87 40 60 68.91 80:20:140]);
xticklabels({'20', '', '40', '60', '', '80', '100', '120', '140'})
yticklabels({'0.0008', '0.0010', '0.0012', '0.0014', '', '', '0.0020', '0.0022', '0.0024'})

ax1 = axes('Position',[0 0 1 1],'Visible','off');
axes(ax1) % sets ax1 to current axes
text(.1,0.49,'L_2', 'Color', 'red', 'Fontsize', 20)
text(.1,0.63,'L_1', 'Color', 'red', 'Fontsize', 20)
text(.2,0.07,'r_1', 'Color', 'red', 'Fontsize', 20)
text(.44,0.07,'r_2', 'Color', 'red', 'Fontsize', 20)
print('-depsc', 'critical_L.eps')


