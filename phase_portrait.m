%%%%% Draw the phase portrait for a point L between the critical points L_1 and L_2 %%%%%
% 8/05/2017

function[] = complete_diagram

    clf

    %%%% parameters of the model
    L = 1.63*10^(-3); % we fixe the value of L (extracellular lipid amount)
    a = 0.5; % lipogenesis maximal flux
    kappa_r = 200;
    n = 3;
    kappa_l = 0.001; % same as in figure 5 of the paper by Soula
    B = 125;
    b = 0.27;
    kappa_t = 0.025; % same as in black curve in figure 3
    tau = 0.1;
    V_l = 1.091*10^6;
    V_0 = 14137; % such that r_0 = 20

    
    %%%% Equilibrium
    l_equi = @(r) (4*pi*r.^3-3*V_0)/(3*V_l); % function to find the value of l at equilibrium given r
    v = [-b*4*pi
            0
            -B*4*pi
            b*3*V_0+ a*(L/(L+kappa_l))*(kappa_r)^3*4*pi - b*4*pi*(kappa_r)^3
            0
            -B*4*pi*(kappa_r)^3-3*V_0
            a*(L/(L+kappa_l))*(kappa_r)^3*3*V_l*kappa_t-3*V_0*a*(L/(L+kappa_l))*(kappa_r)^3+b*3*V_0*(kappa_r)^3
            0
            B*3*V_0*(kappa_r)^3]; % coefficients of the polynomial
    res = roots(v);
    res_real = res(imag(res)==0);
    fixed_r = res_real((res_real)>=0); %keep only positive real roots

    fixed_l = l_equi(fixed_r); % find the value of l at the equilibrium
    
    
    
    %%%% trajectories from different initial conditions
    r_t0 = linspace(20,100,5);
    l_t0 = linspace(0.1,3.5,5);
    tspan = [0 10];
    
    hold on
    for i = l_t0
        for j = r_t0
            CI = [i;j]; % initial conditions
            [t,xx] = ode45(@monedo,tspan,CI);
            plot(xx(:,2),xx(:,1), 'Color', 'b')
        end
    end
   
    
    
    delta = 0.05;
    [V, D] = linearization(fixed_l(2), fixed_r(2), L); % eigenvectors needed to find the directions
    
    %%%% backwards_integration to find the stable manifolds
    %options = odeset('OutputFcn',@odeplot,'OutputSel',[1 2]);
    
    % top left stable manifold
    tspan = 0.5:-0.001:0;
    CI = [fixed_l(2) fixed_r(2)] - delta* V(:,1)';
    [t, xx] = ode45(@monedo, tspan, CI);
    plot(xx(:,2), xx(:,1), 'g', 'LineWidth', 2)

    % bottom right stable manifold
    %tspan = 0.025:-0.0002:0;
    tspan = 0.025:-0.0002:0;
    CI = [fixed_l(2) fixed_r(2)- 0.1] + delta* V(:,1)';
    %[t,xx] = ode23(@monedo,tspan,CI,options);
    [t,xx] = ode45(@monedo,tspan,CI);
    plot(xx(:,2),xx(:,1),'g','LineWidth',2)


    
    %%%% forward_integration to find the unstable manifold
    
    tspan = 0:0.001:2; % tspan for upper right unstable manifold
    CI = [fixed_l(2) fixed_r(2)] - delta* V(:,2)'; % initial conditions for the upper right unstable manifold
    [t,xx] = ode23(@monedo,tspan,CI);
    plot(xx(:,2),xx(:,1),'r','LineWidth',2)

    tspan = 0:0.001:3; % tspan for lower left unstable manifold
    CI = [fixed_l(2) fixed_r(2)] + delta* V(:,2)'; % initial conditions for the lower left unstable manifold
    [t,xx] = ode23(@monedo,tspan,CI);
    plot(xx(:,2),xx(:,1), 'r', 'LineWidth', 2)
    

    
    % plot equilibrium afterwards because it looks nicer
    plot(fixed_r([1 3]),fixed_l([1 3]),'o','MarkerFaceColor','k', 'LineWidth', 2, 'Color', 'k')
    plot(fixed_r(2),fixed_l(2),'o','MarkerFaceColor','w', 'LineWidth', 2, 'Color', 'k')
    %title('Phase portrait for a given extracellular lipid content L = 1.63*10^{-3}')
    xlabel('Adipocyte radius r')
    ylabel('Intracellular lipid content l')
    set(gca, 'FontSize', 20)
    grid on
    
    % legend
    
    hold on;

    h = zeros(3, 1);    
    h(1) = plot(NaN,NaN, 'Color', 'b');
    h(2) = plot(NaN,NaN, 'g','LineWidth',2);
    h(3) = plot(NaN,NaN, 'r','LineWidth',2);
    h(4) = plot(NaN,NaN, 'o','MarkerFaceColor','k', 'LineWidth', 2, 'Color', 'k');
    h(5) = plot(NaN,NaN, 'o','MarkerFaceColor','w', 'LineWidth', 2, 'Color', 'k');

    hleg = legend(h, 'Orbits','Stable manifold','Unstable manifold','Stable equilibrium', 'Unstable equilibrium');
    set(hleg,'Location', 'northeastoutside')
    
    % print('-depsc', 'phase_portrait.eps')

    function xxp = monedo(~,xx)  % one cell model
        % variables
        l = xx(1);
        r = xx(2);
        
        xxp = [ a*r^2*(L/(L+kappa_l))*(kappa_r)^n/((kappa_r)^n+r^n) - (B+b*r^2)*l/(kappa_t+l) ;
            (1/tau)*((V_l*l+V_0)/(4*pi*r^2)-r/3)];     
    end

end